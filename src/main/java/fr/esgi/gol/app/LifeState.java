package fr.esgi.gol.app;

public interface LifeState {

    boolean isAlive();
    boolean shouldSwitchState(Cell cell);
}
