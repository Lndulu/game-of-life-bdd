package fr.esgi.gol.app;

import java.util.HashMap;
import java.util.Map;

public class AliveState implements LifeState {

    @Override
    public boolean isAlive() {
        return true;
    }

    @Override
    public boolean shouldSwitchState(Cell cell) {
        final int aliveNeighborsCount = cell.countAliveNeighbors();
        return (aliveNeighborsCount < 2 || aliveNeighborsCount > 3);
    }
}
