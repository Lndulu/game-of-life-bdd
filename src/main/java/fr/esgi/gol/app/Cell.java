package fr.esgi.gol.app;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;


public class Cell {
    private ArrayList<Cell> neighbors;
    private LifeState state;
    private boolean shouldSwitch;

    public static Map<Character, Boolean> tokens = new HashMap<>();

    static {
        tokens.put('x', true);
        tokens.put('.', false);
    }

    public Cell( boolean alive) {
        this.state = alive ? new AliveState() : new DeadState();
    }

    public Cell( Character aliveToken) {
        this.state = tokens.get(aliveToken) ? new AliveState() : new DeadState();
    }

    public void setNeighbors(ArrayList<Cell> neighbors) {
        this.neighbors = neighbors;
    }

    public boolean shouldSwitch() {
        return shouldSwitch;
    }

    public boolean isAlive() {
        return this.state.isAlive();
    }

    int countAliveNeighbors() {
        int count = 0;
        for (Cell neighbor : neighbors) {
            if (neighbor.state.isAlive()) {
                count ++;
            }
        }
        return count;
    }

    public void update() {
        this.shouldSwitch = state.shouldSwitchState(this);
    }


    public void reset() {
        if (this.shouldSwitch) {
            if (this.isAlive()) {
                this.state = new DeadState();
            } else {
                this.state = new AliveState();
            }
        this.shouldSwitch = false;
        }
    }

    @Override
    public String toString() {
        return tokens
                .entrySet()
                .stream()
                .filter(entry -> isAlive() == (entry.getValue()))
                .map(Map.Entry::getKey)
                .findFirst()
                .toString();
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cell cell = (Cell) o;
        return shouldSwitch == cell.shouldSwitch &&
                Objects.equals(neighbors, cell.neighbors) &&
                Objects.equals(state, cell.state);
    }

    @Override
    public int hashCode() {
        return Objects.hash(neighbors, state, shouldSwitch);
    }
}
