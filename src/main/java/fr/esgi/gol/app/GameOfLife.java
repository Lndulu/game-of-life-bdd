package fr.esgi.gol.app;

import java.util.ArrayList;
import java.util.List;

public class GameOfLife {

    Cell[][] cellGrid;

    public GameOfLife(final boolean[][] grid) {
        this.cellGrid = new Cell[grid.length][grid[0].length];
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[0].length; j++) {
                this.cellGrid[i][j] = new Cell(grid[i][j]);
            }
        }

        for (int i = 0; i < cellGrid.length; i++) {
            for (int j = 0; j < cellGrid[0].length; j++) {
                this.cellGrid[i][j].setNeighbors(this.getNeighborsFromPos(i, j));
            }
        }
    }

    public GameOfLife(final List<List<String>> grid) {
        this.cellGrid = new Cell[grid.size()][grid.get(0).size()];
        for (int i = 0; i < grid.size(); i++) {
            for (int j = 0; j < grid.get(0).size(); j++) {
                this.cellGrid[i][j] = new Cell(grid.get(i).get(j).charAt(0));
            }
        }

        for (int i = 0; i < cellGrid.length; i++) {
            for (int j = 0; j < cellGrid[0].length; j++) {
                this.cellGrid[i][j].setNeighbors(this.getNeighborsFromPos(i, j));
            }
        }
    }

    public boolean[][] passGeneration() {
        boolean[][] grid = new boolean[this.cellGrid.length][this.cellGrid[0].length];

        for (Cell[] cells : this.cellGrid) {
            for (int j = 0; j < this.cellGrid[0].length; j++) {
                cells[j].update();
            }
        }

        for (int i = 0; i < this.cellGrid.length ; i++) {
            for (int j = 0; j < this.cellGrid[0].length; j++) {
                Cell cell = cellGrid[i][j];
                if (!cell.shouldSwitch()) {
                    grid[i][j] = cell.isAlive();
                } else {
                    grid[i][j] = !cell.isAlive();
                }
                cell.reset();
            }
        }

        return grid;
    }

    private ArrayList<Cell> getNeighborsFromPos(int row, int col) {
        ArrayList<Cell> neighbors = new ArrayList<>();
        if (row -1 >= 0) {
            if (col - 1 >= 0) {
                neighbors.add(this.cellGrid[row - 1][col - 1]);
            }
            neighbors.add(this.cellGrid[row - 1][col]);
            if (col + 1 < this.cellGrid[row].length) {
                neighbors.add(this.cellGrid[row - 1][col + 1]);
            }
        }
        if (col - 1 >= 0) {
            neighbors.add(this.cellGrid[row][col - 1]);
        }
        if (col + 1 < this.cellGrid[row].length) {
            neighbors.add(this.cellGrid[row ][col + 1]);
        }
        if (row + 1 < this.cellGrid.length) {
            if (col - 1 >= 0) {
                neighbors.add(this.cellGrid[row + 1][col - 1]);
            }
            neighbors.add(this.cellGrid[row + 1][col]);
            if (col + 1 < this.cellGrid[row].length) {
                neighbors.add(this.cellGrid[row + 1][col + 1]);
            }
        }
        return neighbors;
    }



    public Cell[][] getCellGrid() {
        return cellGrid;
    }


}
