package fr.esgi.gol.app;

public class DeadState implements LifeState {
    @Override
    public boolean isAlive() {
        return false;
    }

    @Override
    public boolean shouldSwitchState(Cell cell) {
        final int aliveNeighborsCount = cell.countAliveNeighbors();
        return (aliveNeighborsCount == 3);
    }
}
