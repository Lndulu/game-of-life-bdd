package fr.esgi.gol.features;

import fr.esgi.gol.app.GameOfLife;

import java.util.List;

public class FeatureContext {

    private GameOfLife game = null;

    public void setGameOfLife(final List<List<String>> textGrid) {
        game = new GameOfLife(textGrid);
    }

    public GameOfLife game() {
        return game;
    }
}
