package fr.esgi.gol.features;

import fr.esgi.gol.app.Cell;
import fr.esgi.gol.app.GameOfLife;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.assertj.core.api.Assertions;

import java.util.Arrays;
import java.util.List;

public class GameOfLifeSteps {


    private final FeatureContext featureContext;

    public GameOfLifeSteps(FeatureContext featureContext) {
        this.featureContext = featureContext;
    }


    @Given("the grid is :")
    public void theGridIs(DataTable dataTable) {
        featureContext.setGameOfLife(dataTable.asLists());
    }

    @When("a generation passes")
    public void aGenerationPasses() {
        featureContext.game().passGeneration();
    }

    @Then("the central cell should be dead") // nb : given the grid is 3X3
    public void theCentralCellShouldBeDead() {
        Cell centralCell = featureContext.game().getCellGrid()[1][1];
        Assertions.assertThat(!centralCell.isAlive());
    }

    @Then("the central cell should be alive")
    public void theCentralCellShouldBeAlive() {
        Cell centralCell = featureContext.game().getCellGrid()[1][1];
        Assertions.assertThat(centralCell.isAlive());
    }

    @When("{int} generations pass")
    public void generationsPass(int generationCount) {
        for (int i = 0; i < generationCount; i++) {
            featureContext.game().passGeneration();
        }
    }

    @Then("the grid should be :")
    public void theGridShouldBe(DataTable dataTable) {
        final List<List<String>> expectedGrid = dataTable.asLists();
        GameOfLife expectedGame = new GameOfLife(expectedGrid);
        Assertions.assertThat(Arrays.deepEquals(expectedGame.getCellGrid(), featureContext.game().getCellGrid()));
    }
}
