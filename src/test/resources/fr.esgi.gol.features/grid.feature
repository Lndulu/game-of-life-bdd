Feature: global grid behavior

  la grille évolue globalement, et les cellules peuvent toutes évoluer à chaque génération

  Scenario : each cell is individually affected
    Given the grid is :
      | x | x | . | . | . |
      | x | x | . | . | . |
      | x | x | . | . | . |
      | x | x | . | . | . |
      | x | x | . | . | . |
    When a generation passes
    Then the grid should be :
      | x | x | . | . | . |
      | . | . | x | . | . |
      | . | . | x | . | . |
      | . | . | x | . | . |
      | x | x | . | . | . |

  Scenario : the grid is affected gradually with 2 generations
    Given the grid is :
      | x | x | . | . | . |
      | x | x | . | . | . |
      | x | x | . | . | . |
      | x | x | . | . | . |
      | x | x | . | . | . |
    When 2 generations pass
    Then the grid should be :
      | . | x | . | . | . |
      | . | . | x | . | . |
      | . | x | x | x | . |
      | . | . | x | . | . |
      | . | x | . | . | . |

  Scenario : the grid is affected gradually with 3 generations
    Given the grid is :
      | x | x | . | . | . |
      | x | x | . | . | . |
      | x | x | . | . | . |
      | x | x | . | . | . |
      | x | x | . | . | . |
    When 3 generations pass
    Then the grid should be :
      | . | . | . | . | . |
      | . | . | x | x | . |
      | . | x | . | x | . |
      | . | . | x | x | . |
      | . | . | . | . | . |

  Scenario : some grid configuration create a cycle
    Given the grid is :
      | . | x | x | x | . |
      | x | . | x | . | x |
      | x | . | x | . | x |
      | . | x | x | x | . |
    When 2 generations pass
    Then the grid should be :
      | . | x | x | x | . |
      | x | . | x | . | x |
      | x | . | x | . | x |
      | . | x | x | x | . |


