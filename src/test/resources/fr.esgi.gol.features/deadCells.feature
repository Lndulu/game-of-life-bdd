
Feature: Dead Cell behavior

à chaque génération
une cellule morte redevient vivante si elle a exactement 3 voisins vivants

  Scenario : dead cell stays dead because of underpopulation
    Given the grid is :
      | x | . | . |
      | . | . | . |
      | . | . | . |
    When a generation passes
    Then the central cell should be dead

  Scenario : dead cell stays dead because of overpopulation
    Given the grid is :
      | x | x | x |
      | x | . | x |
      | . | x | . |
    When a generation passes
    Then the central cell should be dead

  Scenario : dead cell becomes alive by osmosis
    Given the grid is :
      | . | . | . |
      | x | . | x |
      | . | x | . |
    When a generation passes
    Then the central cell should be alive