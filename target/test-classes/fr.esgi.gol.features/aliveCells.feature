Feature: Alive Cell behavior

  les cellules ont deux états
  à chaque génération du jeu,
  une cellule vivante reste vivante uniquement si elle a 2 à 3 voisins vivants

  Scenario : Alive cell dies because no neighbor is alive
    Given the grid is :
      | . | . | . |
      | . | x | . |
      | . | . | . |
    When a generation passes
    Then the central cell should be dead

  Scenario : Alive cell dies because of underpopulation
    Given the grid is :
      | . | . | x |
      | . | x | . |
      | . | . | . |
    When a generation passes
    Then the central cell should be dead

  Scenario : Alive cell stays alive when enough population
    Given the grid is :
      | . | . | x |
      | . | x | x |
      | . | x | . |
    When a generation passes
    Then the central cell should be alive

  Scenario : Alive cell dies because of overpopulation
    Given the grid is :
      | . | x | x |
      | x | x | x |
      | . | x | x |
    When a generation passes
    Then the central cell should be dead
